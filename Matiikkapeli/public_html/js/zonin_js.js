


$(document).ready(function () {
    let pisteet = 0;
    $(".answer").click(function () {
        let vastaus = Number($(this).val());
        let kysymys = $(this).attr("name");
        // $([name=xxx])
        $("[name=" + kysymys + "]").prop("disabled", true);

        $("#" + kysymys + "_explanation").show();

        if (vastaus === 1) {
            // lihavoidaan seuraavan elementin teksti
            $(this).next().addClass("vastaus_teksti");
            // muutetaan taustaväriä parent-elementin avulla
            $(this).parent().addClass("oikein");
            $("#correct").show();
            $("#stars").append('<span class="fa fa-star star">' + '</span>');
            pisteet += 1;

        } else {
            $(this).next().addClass("vastaus_teksti");
            $(this).parent().addClass("vaarin");
            // haetaan oikea vastaus, lihavoidaan  seuraavan elementin teksti
            $("[name=" + kysymys + "][value=1]").next().addClass("oikea_vastaus");
            $("#incorrect").show();
            pisteet += 0;
        }

    });
    // version 3 laajennus, kysymykset sekalaisessa järjestyksessä
    kysymykset3 = ["#kysymys1", "#kysymys2", "#kysymys3", "#kysymys4", "#kysymys5", "#kysymys6", "#kysymys7", "#kysymys8"];

    $(".nappula").click(function () {
        $("#correct").hide();
        $("#incorrect").hide();
        if (kysymykset3.length > 0) {
            let kysymys = getRndInteger(0, kysymykset3.length - 1);
            let esille = kysymykset3[kysymys];
            $(esille).show();
            kysymykset3.splice(kysymys, 1);
            //painikkeen vanhempi piilotetaan
            $(this).parent().hide();

        } else {
            //mitä tehdään kun ei ole enää kysymyksiä
            $(this).parent().hide();
            $("#lopetustekstit").show();
            $("#pisteet").html(pisteet);
            $("#alkuun").show();
            if (pisteet > 7) {
                $("#miten_meni").html("KAIKKI OIKEIN! olet nero!");
            } else if (pisteet > 5) {
                $("#miten_meni").html("Eläin tietoutta löytyy." + "<i class='fa fa-thumbs-o-up'></i>");
            } else if (pisteet > 3) {
                $("#miten_meni").html("Visa taisi olla hieman haastava.");
            } else {
                $("#miten_meni").html("Ei mennyt ihan putkeen tällä kertaa.");
            }
        }
    });
    $("#alkuun").click(function () {
        location.reload();
    });
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }
});