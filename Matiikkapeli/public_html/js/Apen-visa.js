/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
    let tulos = 0;
    
    kysymykset3 = ["#kysymys1", "#kysymys2", "#kysymys3","#kysymys4","#kysymys5","#kysymys6","#kysymys7","#kysymys8"];
    
    // vaihtoehdon valinta
   $(".answer").click(function(){
     let valinta = Number($(this).val());
     
     let kys = $(this).attr("name");
     $("[name=" + kys +"]").prop("disabled", true);
     
     // vastauksen valinta, selityksen näyttäminen ja pistelaskuriin lisäys kun vastaa oikein
     $("#" + kys + "_explanation").show();
     if (valinta === 1){
         $(".kuva").html('<span><img src="kuvat/thumbs-up-31663_960_720.png" class="img-fluid" alt=""/></span>');
         $(this).parent().addClass("oikein");
         $("[name=" + kys + "][value=1]").next().addClass("oikea_vastaus");
         tulos += 1;
         console.log(tulos);
         $(stars).append('<span class="fa fa-star star">'+'</span>');
     } else {
         $(".kuva").html('<span><img src="kuvat/disapprove-149251_960_720.png" class="img-fluid" alt=""/></span>');
         $(this).next().addClass("vastaus_teksti");
         tulos += 0;
         //haetaan oikea vastaus, lihavoidaan seuraavan elementin (tässä vastaus) teksti
         $("[name=" + kys + "][value=1]").next().parent().addClass("oikea_vastaus");
     }
   }) ;

   // Seuraavan kysymyksen näyttäminen satunnaisesti valittuna
   $(".seuraava3").click(function(){
       
       if (kysymykset3.length > 0) {
       let kysymys = getRndInteger(0, kysymykset3.length-1);
       let esille = kysymykset3[kysymys];
       $(esille).show();
       kysymykset3.splice(kysymys,1);
       //painikkeen parent (kysymys) piiloitetaan.
       $(this).parent().hide();
   } else {
       // Kysymykset loppu ja loppunäkymän näyttäminen
       $(this).parent().hide();
       $("#lopputulos").show();
       if(tulos > 6){
        $("#loppusana").html("Erinomaista tietotaitoa!");
    } else if (tulos > 4) {
        $("#loppusana").html("Petrattavaa olisi, mutta hyvä yritys!");
    } else {
        $("#loppusana").html("Oivoi, et taida olla sarjisfani :(");
    }
   }
});
   
// Sivun aloitusnäkymään palaaminen
$(".alkuun").click(function(){
    location.reload();
});
function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1) ) + min;
    }
});




