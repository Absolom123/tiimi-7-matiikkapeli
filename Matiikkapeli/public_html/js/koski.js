//Sami Koski

$(document).ready(function () {
    //Oikein vastatuiden kysymysten määrä
    let score = 0;

    // Random Number Generator
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1) ) + min;
    }

    //Vastauksen klikkausfunktio
    $(".answer").click(function () {
        let answer = Number($(this).val());
        let question = $(this).attr("name");
        $("[name=" + question + "]").prop("disabled", true);

        //Kysymyksen selityksen näyttäminen
        $("#" + question + "_explanation").show();

        //Jos vastaus on oikein
        if (answer === 1) {
            //Lihavoidaan vastaus.
            $(this).next().addClass("answer");

            //Muutetaan vastauksen taustaväriä (vanhempi)
            $(this).parent().addClass("correct");

            //Lisätään scorea ja appendataan tähti kysymyssivulle
            score++;
            $(stars).append('<span class="fa fa-star star">'+'</span>');

        //Jos vastaus on väärin
        } else {
            //Lisätään answer class vastaukseen
            $(this).next().addClass("answer");

            //Lisätään vastaukseen incorrect class
            $(this).parent().addClass("incorrect");

            //Lisätään oikeaan vastaukseen correct_answer class
            $("[name=" + question + "][value=1]").next().addClass("correct_answer");
        }

        //Jos alle 2 kysymykseen on vastattu oikein, tulosta "vähän huonosti."
        if (score <= 2) {
            $("#result").html("vähän huonosti.");
        }
        //Jos alle 4 mutta yli 2 kysymykseen on vastattu oikein, tulosta "hyvin."
        if (score < 4 && score > 2) {
            $("#result").html("hyvin.");
        }
        //Jos yli 4 vastattu oikein, tulosta "erinomaisesti."
        if (score > 4) {
            $("#result").html("erinomaisesti.");
        }

    });

    //Kysymys array
    questions = ["#question1","#question2","#question3","#question4","#question5","#question6"];

    //nextButton klikkausfunktio
    $(".nextButton").click(function () {
        //Jos kysymysten määrä on yli nolla
        if (questions.length > 0) {
            
            //Valitaan kysymys sekalaisessa järjestyksessä
            let question = getRndInteger(0, questions.length-1);
            
            //Näytetään valittu kysymys
            let showQuestion = questions[question];
            $(showQuestion).show();
            
            //Poistetaan valittu kysymys arraysta
            questions.splice(question, 1);
            
            //Piilotetaan napin vanhempi (aikaisempi kysymys)
            $(this).parent().hide();
        
        //Jos kysymysten määrä on 0 tai alle
        } else {
            //Piilotetaan napin vanhempi (aikaisempi kysymys) ja näytetään loppuruutu
            $(this).parent().hide();
            $(finished).show();
        }
    });

    //retryButton Klikkausfunktio
    $(".retryButton").click(function () {
        //Reloadaa sivun
        location.reload();
    });

});