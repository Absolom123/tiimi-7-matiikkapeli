//Sami Koski

$(document).ready(function () {
    
    let divider = [2, 4]; // Jakajat
    let divisable = [8, 12, 16, 20, 24, 28, 32]; // Jaettavat

    let amountOfTasks = 0; // Tehtävien määrä
    let taskNum = 0; // Tällähetkellä aktiivinen tehtävä

    // Random Number Generator
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1) ) + min;
    }

    //Funktio joka luo tehtävän
    function createTask() {
        //Arrayt josta tehtävissä olevat luvut arvotaan.
        taskDivisable = divisable[getRndInteger(0, divisable.length-1)];
        taskDivider = divider[getRndInteger(0, divider.length-1)];

        //Tulostetaan tehtävät.
        $("#divisable").html(taskDivisable);
        $("#divider").html(taskDivider);

        //Lisätään tehtävänumeroa
        taskNum++;
        
        //Tulostetaan tehtävänumerot
        $("#currentTask").html(taskNum);
        $("#taskAmount").html(amountOfTasks);

    }

    //Napin klikkaus
    $(".nextButton").click(function () {

        //Otetaan valittu määrä tehtäviä
        amountOfTasks = $(selectAmount).val();

        //Jos aloitusruutu on näkyvissä, piilota se ja laita tehtävä näkyväksi.
        if($("#main").is(":visible")){

            $("#exersize").show();
            $("#main").hide();
            //Luo ensimmäinen tehtävä
            createTask();

        } else {

            if (taskNum < amountOfTasks) {
                //Vastaus variablet
                let answer = taskDivisable / taskDivider;
                let userAnswer = $("#userInput").val();
                
                //Jos vastaus on oikein, tulosta tähti, näytä modal ja luo uusi tehtävä.
                if (answer == userAnswer) {
                    $("#message").modal();
                    $(stars).append('<span class="fa fa-star star">'+'</span>');
                    createTask();
                
                //Jos tehtävä on väärin, näytä modal ja luo uusi tehtävä
                } else {
                    $("#error_message").modal();
                    createTask();
                }
            
            // Kun tehtäviä ei ole, lopeta peli ja näytä loppuruutu
            } else {
                $("#end").show();
                $("#exersize").hide();
            }

        }
        
    });
    
    //Refreshaa sivu, kun viimeistä nappia painetaan
    $(".refreshButton").click(function () {
        location.reload();
    });

});