/*
 * Student name : Pyry Hofslagare
 * Start date : 19/11/2019
 * End date: 17/12/2019
 */

$(document).ready(function () {
  //  let pisteet = 0;
 
  //  $(nyt).html(pisteet);
 
    let tulos = 0;
    
    // visa valinta
    $(".svaret").click(function () {
        $("#quizstart2").hide();
        let svar = Number($(this).val());
        let fråga = $(this).attr("name");
        // $([name=xxx])
        $("[name=" + fråga + "]").prop("disabled", true);


        // explanation
        $("#" + fråga + "_beskrivning").show();
        $(".efter3").show();
        if (svar === 1) {
            //  $(this).next().addClass("rätt");

            // lihavoidaan next elementti
            $(this).next().addClass("svar_text");
            // muutetaan taustaväriä parent elementillä
            $(this).parent().addClass("rätt");
            tulos += 1;
            
     //       console.log(tulos);
       //     pisteet += 1;
        } else {
            $(this).next().addClass("svar_text");
            
            $(this).parent().addClass("orätt");
            
            $("[name=" + fråga + "][value=1]").next().addClass("rätt_svar");
        }

        $(currentpoint).html(tulos);
    });

    // version kolme laajennus, kysymykset sekalaisessa järjestyksessä
    frågor3 = ["#fråga1","#fråga2", "#fråga3", "#fråga4", "#fråga5", "#fråga6", "#fråga7", "#fråga8"];
    $(".efter3").click(function () {
        $("#feedback").hide();
          
        if (frågor3.length > 0) {
        let fråga = getRndInteger(0, frågor3.length-1);
        let synas = frågor3[fråga];
        $(".efter3").hide();
        $(synas).show();
        frågor3.splice(fråga, 1);
        
        

        // painikkeen vanhempi piilotetaan
     //   $(this).parent().hide();
     //   } else {


            // mitä tehdään, kun ei ole enää kysymyksiä?
       //     alert("Olet vastannut kaikkiin kysymyksiin. Paina OK palataksesi visaan.");
        }
        else {
            $("#feedback").show();
            $("#quizstart2").hide();
        }      
        $(this).parent().hide();
   
               
        if (tulos <= 2) {
            $("#feedback").html("Suoriuduit melko heikosti. Yritähän vielä uudelleen.");
        }

        else if (tulos == 3) {
            $("#feedback").html("Suoriuduit huonosti, mutta ei se mitään. Voit yrittää uudelleen.");
        }
        
        else if (tulos == 4) {
            $("#feedback").html("Suoriuduit välttävästi. Tiedät kuitenkin yleistasolla paremmin kuin moni muu!");
        }

        else if (tulos == 5) {
            $("#feedback").html("Suoriuduit tyydytttävästi, mutta kuitenkin paremmin kuin puolet vastaajista!.");
        }

        else if (tulos == 6) {
            $("#feedback").html("Suoriuduit hyvin. Tässä vaiheessa voi olla jo vähän ylpeä!");
            $("#feedback").append("<br><br><img src='./kuvat/bronzemedal.png' alt='image 1' style='width:350pxpx;height:350px;'>");

        }
  
        else if (tulos == 7) {
            $("#feedback").html("Suoriuduit erinomaisesti, huippu juttu! Vain yksi kysymys väärin.");
            $("#feedback").append("<br><br><img src='./kuvat/silvermedal.png' alt='image 1' style='width:350pxpx;height:350px;'>");

        }
    
        else if (tulos == 8) {
            $("#feedback").html("Suoriuduit täydellisesti. Mahtavaa! Tiedät yleistasolla todella paljon!");
            $("#feedback").append("<br><br><img src='./kuvat/goldmedal.png' alt='image 1' style='width:350pxpx;height:350px;'>");
        }

   //     $(this).parent().hide();
   $("#quizstart2").show();
    //    alert("Olet vastannut kaikkiin kysymyksiin. Paina OK palataksesi visaan.");
    });

// start
  //  $(this).parent().hide();
    $("#quizstart2").show();
    $(".startaåter").click(function(){
    location.reload();
});

// random integer

    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1) ) + min;
      }
});