/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    let valittumaara = 0;
    let teht = 1;
    let skaala1 = 0;
    let skaala2 = 0;
    let tulos = 0;

    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
// valitaan haluttu määrä tehtäviä ja näytetään ensimmäinen tehtävä
    $(".aloita").click(function () {

        $("#tehtava").show();
        $("#aloitusruutu").hide();

        valittumaara = $("#maara").val();

        $("#kaikki").html(valittumaara);

         skaala1 = getRndInteger(1, 9);
         skaala2 = getRndInteger(1, 9);

        $(".eka").html(skaala1);
        $(".toka").html(skaala2);
    });

// Katsotaan onko vastaus oikein ja jos on printataan tähti, jos väärin niin alert viesti. 
// Lisätään tehtävämäärää aina yhdellä. 
// Printataan seuraava tehtävä.
    $(".seuraava").click(function () {

        if (teht <= valittumaara) {

            let oikein = skaala1 * skaala2;
            let vastaus = Number($("#vastaus").val());
            
            $(".eka").html(skaala1);
            $(".toka").html(skaala2);

            if (vastaus === oikein) {
                tulos++;
                $("#stars").append('<span class="fa fa-star star">' + '</span>');
            }else {
                alert("väärin meni");
                
            }
            skaala1 = getRndInteger(1, 9);
           skaala2 = getRndInteger(1, 9);

          $(".eka").html(skaala1);
          $(".toka").html(skaala2);
          teht += 1;
          $("#nyt").html(teht);
        }
        // Tehtäviä ei enää ole, piiloitetaan tehtävä osio ja näytetään loppunäkymä
        // Erilaiset kommentit tuloksesta eri pistemäärillä
            if (teht >valittumaara) {
            $("#tehtava").hide();
            $("#lopputulos").show();
            
            if (tulos === Number(valittumaara)) {
                $("#loppusana").html("Sinullahan on laskupäätä!");
            }else if (tulos === 0 | tulos === 1){
                $("#loppusana").html("Kuka ei ole ollut hereillä tunneilla?");
            }  else if (valittumaara === "9"){
                 if (tulos < 4){
                    $("#loppusana").html("Sinulla ei taida olla laskupäätä");
                }
                else if (tulos < 6){
                    $("#loppusana").html("Hyvä yrityas!");
                } 
            } else if (valittumaara === "6"){
                if (tulos < 4) {
                    $("#loppusana").html("Hyvä yritys!");
                }
            } else if (tulos <= valittumaara - 1) {
                $("#loppusana").html("Käyppäs vielä kertaamassa, niin ensikerralla saat täydet pisteet!");
            }
        }
    });
    
    // Vastauskentän kohdistaminen ja maalaus
    $("#vastaus").focusin(function () {
        $("#vastaus").select();
    });
    
    // Paluu aloitusnäkymään
    $(".alkuun").click(function () {
        location.reload();
    });
});


