$(document).ready(function() {

    let pistemaara = 0;

    let maxpisteet = 8;

    $(uusi).html(pistemaara);
    $(numerot).html(maxpisteet);
    
    let tulos = 0;


    $(".answer").click(function(){
        // lue value-attribuutin arvo muuttujaan, numeerisena
        let vastaus = Number($(this).val());

        let kysymys = $(this).attr("name");
        // $([name=xxx])
        $("[name=" + kysymys + "]").prop("disabled", true);

        // $("#xxx_explanation")
        $("#" + kysymys + "_explanation").show();

        if (vastaus === 1) {
            //lihavoidaan seuraavan elementin teksti
           $(this).next().addClass("vastaus_teksti");
           // muutetaan taustaväriä parent-elementin avulla
           $(this).parent().addClass("oikein");
           //lasketaan pisteitä
           tulos += 1;
           $("#uusi").html(tulos);
           pistemaara += 1;
        } else {
           $(this).next().addClass("vastaus_teksti");
           $(this).parent().addClass("vaarin");
            //haetaan oikea vastaus, lihavoidaan seuraavan elementin teksti
        $("[name=" + kysymys + "][value=1]").next().addClass("oikea_vastaus");
        }

        
        $(uusi).html(pistemaara);
        $(numerot).html(maxpisteet);
    });

    // version kaksi laajennus, kysymykset järjestyksessä yksi kerrallaan
    kysymykset = ["#kys2", "#kys3"];

    $(".seuraava").click(function(){
        if (kysymykset.length > 0) {
            let esille = kysymykset[0];
            $(esille).show();
            kysymykset.splice(0,1);
            // painikkeen vanhempi 
            $(this).parent().hide();
        } else {
            // mitä tehdään, kun ei ole enää kysymyksiä?
        }
    });

    // version 3 laajennus, kysymykset sekalaisessa järjestyksessä
    kysymykset3 = ["#kys1", "#kys2", "#kys3", "#kys4", "#kys5", "#kys6", "#kys7", "#kys8"];

    $(".seuraava3").click(function(){
        if (kysymykset3.length > 0) {
            let kysymys = getRndInteger(0,kysymykset3.length-1);
            let esille = kysymykset3[kysymys];
            $(esille).removeClass("hided");
            $(esille).show();
            kysymykset3.splice(kysymys,1);
            
            // painikkeen vanhempi 
            $(this).parent().hide();
        } else {

            $("#lopputulos").removeClass("piilossa");
            if (tulos <= 3) {
                $("#lopputulos").html("Et taida kuunnella räp-musiikkia?");
            }
    
            if (tulos < 6 && tulos > 3) {
               $("#lopputulos").html("Taidat tykätä räp-musiikista mutta tietämyksesi ei ollut täydellistä");
           }
    
           if (tulos > 6) {
               $("#lopputulos").html("Hienoa! Olet oikea räp-tietäjä!");
           }
        }
    });

    //RNG
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1) ) + min;
      }


     //refreshaa sivuston
     $("#restartgame").click(function () {
        location.reload();
    });
});


