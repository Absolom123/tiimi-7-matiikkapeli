/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
    let pisteet = 0;
    let mpisteet = 8;

    $(nyt).html(pisteet);
    $(numero).html(mpisteet);
    
    let tulos = 0;
    
    // vaihtoehdon valinta
   $(".answer").click(function(){
     let valinta = Number($(this).val());
     
     let kys = $(this).attr("name");
     $("[name=" + kys +"]").prop("disabled", true);
     
     // vastauksen valinta ja selityksen näyttäminen ja lasketaan samalla pisteitä
     $("#" + kys + "_explanation").show();
     if (valinta === 1){
         $(this).parent().addClass("oikea");
         $("[name=" + kys + "][value=1]").next().addClass("oikeavastaus");
         tulos += 1;
         console.log(tulos);
         $("#vastaukset").html(tulos);
         pisteet += 1;
     } else {
         $(this).parent().addClass("vaarin");
         $(this).next().addClass("vastausteksti");
         
         //haetaan oikea vastaus, lihavoidaan seuraavan elementin teksti
         $("[name=" + kys + "][value=1]").next().parent().addClass("oikeavastaus");
     }
     $(nyt).html(pisteet);
    $(numero).html(mpisteet);
   }) ;
    
   kysymykset = ["#kys1", "#kys2", "#kys3","#kys4","#kys5","#kys6","#kys7","#kys8"];
   
   let maara = kysymykset.length;
   
   // Kysymykset satunnaisessa järjestyksessä
   $(".seuraava").click(function(){
       
       if (kysymykset.length > 0) {
       let kysymys = getRndInteger(0, kysymykset.length-1);
       let esille = kysymykset[kysymys];
       $(esille).show();
       kysymykset.splice(kysymys,1);
       //painikkeen parent (kysymys) piiloitetaan.
       $(this).parent().hide();
   } else {
       // visan loppuminen
       $(this).parent().hide();
       $("#lopputulos").show();
       if(pisteet > 6){
        $("#lopetus").html("Hienosti meni, opitko edes mitään uutta?");
    } else if (pisteet < 6){
        $("#lopetus").html("Ei täydellistä, mutta toivottavasti opit jotain uutta.");
    }
   }
});
    
// uudestaan aloittaminen
$(".alkuun").click(function(){
    location.reload();
});
function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1) ) + min;
    }
});